# MyUW Images

## Purpose

This repository is a place to store images that MyUW uses.  The need comes up
when you want to keep images out of a code repository but don't want to use non-
university hosting service like imgur, flickr, or using git hosting urls.
This is just a simple file structure.

## How to add content

If there is shared content across the MyUW apps use the shared folder in the
content images directory.  If you need the images for a specific module, create
a new folder with your module name and proceed to use a directory structure that
makes sense for your needs.

Note that if you create a new folder, you will need to register it in pom.xml.

If there are shared screenshots images across the MyUW apps, use the shared
folder in the screenshots images directory.  If you need the images for a
specific module, create a new folder with your module name and proceed to use a
directory structure that makes sense for your needs.

## How to use content

Upon a commit to this repository, this repo will be added to the MyUW
environments that use a snapshot of this repository ([predev][] as of now).
All images are located under `/images` using the
file structure of the repo.  Example - `/images/contentImages/careerLocker/careerLockerLoginButton.gif` .

## Things for developers to consider

Changing the file structure of this repository will change the
file structure of the urls.  Proceed with extreme caution.

This repository is only for media files.  Updating binary files often will
result in massive repositories.  Better to use this repository as a repository
of things that do very seldomly change, e.g., logos and screenshots.  Best
practice if you need two logos for different environments temporarily is to add
the new logo with a different file name ex `updated_logo.png`.  This repository
will not use different branches for different uses cases (example different
branches for different tiers).

Be respectful of our users; users don't want to download large files, and we
don't want to store them either.

## Local development

You can serve up the content anyway you want for your applications.

### You can deploy to a running local Tomcat

We added in support to deploy the artifact to Tomcat using Maven. To setup add a
server to your `.m2/settings.xml` for Tomcat.

Example:

```xml
<server>
  <id>TomcatServer</id>
  <username>user</username>
  <password>password</password>
</server>
```

The id of `TomcatServer` is important here. Add that user/pass combo to your
`$TOMCAT_HOME/conf/tomcat-users.xml`. Also be sure you have a `role`  of `manager` listed.

Example:

```xml
<role rolename="manager"/>
<user username="user" password="password" roles="manager-script"/>
```

The `role` of `manager-script` gives them the ability to use the `/text` api from
Tomcat.

Read more about that in the [Tomcat Maven plugin documentation][].


With this you can run `mvn tomcat7:deploy` or `mvn tomcat7:redeploy` if you have
already deployed it once. We also wrote a script for this. Just run `./build.sh`

## Releasing

Routinely, this repository hangs out publishing a SNAPSHOT version.

For instance, as of this writing, it publishes `1.1.17-SNAPSHOT`.

This is handy because MyUW Jenkins monitors this repository and upon change will
build a fresh snapshot release. Jenkins includes that snapshot in the MyUW
predev tier.

So once a change merges to master, that change will build and deploy
and be testable in predev without first having to cut a named versioned release
of this product.

However, to promote a version of MyUWImages beyond predev, you'll need a real
release with a version number.

To cut a release,

```sh
mvn release:prepare
mvn release:perform
```

Per the `pom.xml`, this project publishes its releases to
`artifacts.doit.wisc.edu`.

In the course of that releasing, the Maven command line tool will prompt you to
choose a version number. Practice Semantic Versioning. So,

+ fixing a bugged existing image is a "patch" release
+ adding a new image is a "new feature" "minor" release
+ changing the paths is a "breaking change" "major" release

Coming out the other end of that releasing, the Maven command line tool will set
the new in-development version number to a version number you specify. You
should pick the new SNAPSHOT patch past whatever version you just released.

## Deploying

After releasing a named version and updating the snapshot version that this
project publishes, you'll need to update MyUW's tiers to use the new SNAPSHOT
or release.

[predev should use the new SNAPSHOT][predev overlay] so it continues to run the
latest. [test][test overlay], [qa][qa overlay], and [prod][prod overlay] should
update to use the release.

## Alternatives and futures

MyUWImages is a local implementation of something like a CDN. It might be
worthwhile to someday use a real CDN to deliver images instead.

[predev]: https://predev.my.wisc.edu/images
[predev overlay]: https://git.doit.wisc.edu/myuw-overlay/myuw-ear/blob/predev/pom.xml#L73
[test overlay]: https://git.doit.wisc.edu/myuw-overlay/myuw-ear/blob/test/pom.xml#L261
[qa overlay]: https://git.doit.wisc.edu/myuw-overlay/myuw-ear/blob/qa/pom.xml#L240
[prod overlay]: https://git.doit.wisc.edu/myuw-overlay/myuw-ear/blob/prod/pom.xml#L219

[Tomcat Maven plugin documentation]: http://tomcat.apache.org/maven-plugin-2.0/tomcat7-maven-plugin/plugin-info.html
